var http = require('http');

http.createServer(function (req, res) {
	    res.writeHead(200, {'Content-Type': 'text/plain'});
	    res.end('Hello outside VM World! I am one of two Node.js app created by Ansible.\n');
}).listen(3000, "0.0.0.0");
console.log('Server running at http://0.0.0.0:3000/');